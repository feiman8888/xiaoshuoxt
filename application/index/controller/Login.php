<?php
namespace app\index\controller;
use think\Controller;
use weixin\wx;
use app\index\model\iClient;
use app\common\model\myCache;
use app\index\model\iMember;
use app\common\model\myValidate;
use app\index\model\iMessage;
use think\Db;

class Login extends Controller{
    //登录
    public function index(){
        $device_type = iClient::getDeviceType();
        switch ($device_type){
        	case 1:
        		$this->redirect('User/index');
        		break;
        	case 2:
        		$tpl = 'mobile';
        		break;
        	case 3:
        		$tpl = 'pc';
        		break;
        }
        $code = myCache::createLoginCode();
        $urlData = myCache::getUrlCache();
        $qrcode = self::createLoginQr($code,$urlData);
        $variable = [
        	'code' => $code,
        	'qrcode' => $qrcode,
        	'site_title' =>$urlData['name']
        ];
        $this->assign($variable);
        return $this->fetch($tpl);
    }
    
    //手机号登陆
    public function doLogin(){
    	$rules = [
    		'phone' =>  ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
    		'code' => ['require|number|length:6',['require'=>'请输入验证码','number'=>'验证码格式错误','length'=>'验证码为6位数字']]
    	];
    	$data = myValidate::getData($rules, 'phone,code');
    	iMessage::check($data['phone'], $data['code']);
        //判断是否注册
        $re = Db::name('Member')->where('phone','=',$data['phone'])->value('id');
        if($re){
            //登录
            iMember::phoneLogin($data['phone']);
        }else{
            $this->phoneRegiest($data['phone']);
        }
    }

    //手机注册
    public function phoneRegiest($phone){
        $data = array(
            'channel_id' => 0,
            'agent_id' => 0,
            'wx_id' => 0,
            'headimgurl' => '/static/templet/default/headimg.jpeg',
            'spread_id' => 0,
            'nickname' => $phone,
            'sex' => 0,
            'create_time' => time(),
            'phone'=>$phone,
            'money'=>60
        );
        $re = Db::name('Member')->insertGetId($data);
        if($re){
            if($data['spread_id']){
                Db::name('Spread')->where('id','=',$data['spread_id'])->setInc('visitor_num');
            }
            $member = Db::name('Member')->where('phone','=',$phone)->field('id')->find();
            session('INDEX_LOGIN_ID',$member['id']);
            res_return(['flag'=>22]);
        }else{
            res_return('用户创建失败');
        }
    }
    //创建登陆二维码
    private function createLoginQr($code,$config){
    	wx::$config = $config;
    	$qr_url = wx::createTmpQrcode($code);
    	return $qr_url;
    }
    
    //检查登陆情况
    public function checkQrLogin(){
    	$rules = ['code' => ['require|alphaNum',['require'=>'参数异常','alphaNum'=>'参数格式不规范']]];
    	$code = myValidate::getData($rules,'code');
    	$data = cache($code);
    	if($data){
    		if(is_numeric($data)){
    			iMember::saveLogin($data);
    			cache($code,null);
    			res_return(['url'=>my_url('User/index')]);
    		}else{
    			res_return(['url'=>0]);
    		}
    	}else{
    		res_return('二维码已过期');
    	}
    }
}